
#pragma once

#include "ofMain.h"
#include "ofxRunway.h"
#include "ofxBox2d.h"


class CustomParticle : public ofxBox2dCircle {
public:
    ofColor color;
    
    CustomParticle(b2World * world, float x, float y) {
        setPhysics(0.4, 0.53, 0.31);
        setup(world, x, y, ofRandom(3, 10));
        color.r = ofRandom(20, 100);
        color.g = 0;
        color.b = ofRandom(150, 255);
    }
    
    void draw() {
        float radius = getRadius();
        
        ofPushMatrix();
        ofTranslate(getPosition());
        ofSetColor(color);
        ofFill();
        ofDrawCircle(0, 0, radius);
        ofPopMatrix();
    }
};
class ofApp : public ofBaseApp, public ofxRunwayListener{

    public:
        void setup();
        void update();
        void draw();
        void keyPressed(int key);
        
    ofxRunway runway;
        
        ofImage runwayResult;
        
        ofVideoGrabber cam;
        //ofVideoPlayer video;
        
        // Callback functions that process what Runway sends back
        void runwayInfoEvent(ofJson& info);
        void runwayErrorEvent(string& message);
        float noise;
        float                     px, py;
        bool                                    bDrawLines;
        bool                                    bMouseForce;
        
        ofxBox2d                                box2d;           // the box2d world
       // ofPolyline                              drawing;         // we draw with this first
        ofxBox2dEdge                            edgeLine;        // the box2d edge/line shape (min 2 points)
        vector    <shared_ptr<ofxBox2dCircle> > circles;
        ofxRunwayData::SegmentationMap segmentationMap;
        
};
