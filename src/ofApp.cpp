#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetWindowShape(513,385);
    ofBackgroundHex(0xfdefc2);
    ofSetVerticalSync(true);
    
    runway.setup(this, "http://localhost:8000");
    runway.start();
    
   cam.setup(640,480);
    
    //video.load("test.mov");
    //video.play();
    
    
    ofSetLogLevel(OF_LOG_NOTICE);
    
    box2d.init();
    box2d.setGravity(0, 10);
    box2d.createGround();
    box2d.setFPS(60.0);
    box2d.registerGrabbing();

    
    edgeLine.setPhysics(0.0, 0.5, 0.5);
    edgeLine.create(box2d.getWorld());
}

//--------------------------------------------------------------
void ofApp::update(){
    cam.update();
    //video.update();
    if(cam.isFrameNew()){
        runway.send("image", cam, OFX_RUNWAY_JPG);
        
        
    }
    runway.get("image",runwayResult);
    box2d.update();
    
    ofRemove(circles, ofxBox2dBaseShape::shouldRemoveOffScreen);
}

//--------------------------------------------------------------
void ofApp::draw(){
    //cam.draw(0,0);
    
    ofNoFill();
    ofSetHexColor(0x444342);
    //if(drawing.size()==0) {
    
    //}
    
    //else drawing.draw();
    //float time = ofGetElapsedTimef();
    if(runwayResult.isAllocated()){
        //runwayResult.draw(0,0);
        
        /*
        int x = ofGetMouseX();
        int y = ofGetMouseY();
        if(x < cam.getWidth()){
            // mouse is over the original image
            x = ofMap(x, 0, cam.getWidth(), 0, runwayResult.getWidth());
            y = ofMap(y, 0, cam.getHeight(), 0, runwayResult.getHeight());
        }else{
            //mouse is over the segmentation map
            x -= cam.getWidth();
        }*/
       // auto label = ofxRunwayData::findSegmentationLabel(segmentationMap, runwayResult, x , y );
        
       // if(!label.empty()){
         //   ofDrawBitmapStringHighlight(label, ofGetMouseX()+ 2, ofGetMouseY()+2);
        //}
        for (int i = 0; i < runwayResult.getWidth(); i+= 5) {
          for (int j = 0; j < runwayResult.getHeight(); j+= 5) {
              ofColor color = runwayResult.getPixels().getColor(i, j);
              
              float brightness = color.getBrightness();
              float radius = ofMap(brightness, 0, 255, 0, 8);
              float light = runwayResult.getPixels().getColor(i, j).getLightness();
              auto label = ofxRunwayData::findSegmentationLabel(segmentationMap, runwayResult, i , j);
              if(label == "person"){
                  edgeLine.addVertex(i,j);
              }
          }
        }
        
        
    }
    
    for(auto &circle : circles) {
        ofFill();
        ofSetHexColor(0x90d4e3);
        circle->draw();
    }
    edgeLine.draw();
    edgeLine.setPhysics(0.0, 0.5, 0.5);
    edgeLine.create(box2d.getWorld());
   
}
void ofApp::keyPressed(int key) {

    if(key == 'c') {
        auto circle = make_shared<ofxBox2dCircle>();
        circle->setPhysics(3.0, 0.53, 0.1);
        circle->setup(box2d.getWorld(), mouseX, mouseY, ofRandom(4, 20));
        circles.push_back(circle);
    }
}
//--------------------------------------------------------------
void ofApp::runwayInfoEvent(ofJson& info){
    ofxRunwayData::getSegmentationMap(segmentationMap, info);
}
void ofApp::runwayErrorEvent(string& message){
    ofLogNotice("ofApp::runwayErrorEvent") << message;
}

